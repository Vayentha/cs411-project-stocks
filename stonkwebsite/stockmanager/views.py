from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

from . import serveStocks as ss
import itertools

tickers = ['NVDA', 'AAPL']
db_data = ss.fetch_all_in_table("stocks")

user = "cs 411 test group"

# test stock data for checking formatting on webpage
# TODO automate this dictionary
stocks = ss.build_stock_dict()

# TODO: format stocks correctly on webpage


# templates are in stockmanager/templates/stockmanager/<template name>.html, but you can just pass the subdirectory
# under the templates folder.
def home(request):
    context = {
        'stocks': stocks,    # pass context variables here. Can pass as many as needed to make a nice dynamic webpage
        'user' : user,
        'stocktable': db_data,
    }
    return render(request, 'stockmanager/home.html', context)

def about(request):
    return HttpResponse('<h2> About Page </h2>')


#======================= EDIT LOG =================
# [6-14-2020 -- SAYAN] removed unnecessary tuple code from line 9