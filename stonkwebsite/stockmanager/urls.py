
from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='stocksite-home'),
    path('about/', views.about, name='stocksite-about'),
]
