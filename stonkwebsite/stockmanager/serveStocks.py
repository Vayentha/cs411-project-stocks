import mysql.connector as mysql     #connect to local mysql
# IEXFINANCE imports below
from iexfinance.stocks import Stock
import json                     # parse json library
from pandas import DataFrame    # allows IEX dataframes to be turned into jsons and vice versa
import yaml



# gets SQL data from yaml file(config folder) -- CHANGE THIS TO WHATEVER YOUR FILE CONFIG IS
def getData(item):
    with open(r'/Users/vayentha/cs411-project-stocks/stonkwebsite/stockmanager/config/dbconfig.yaml') as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        return data[item]

# mysql data here
host = f"{getData('ip')}"
db = f"{getData('server')}"
msuser = f"{getData('user')}"
passw = f"{getData('password')}"

iextoken = "pk_6c06024a27b84bc4a2c00ffbebe220c4"
dbc = mysql.connect(host=host, database=db, user=msuser, password=passw) # db connection here

# three important functions


def get_user_stocks(userFN, userLN):
    csr = dbc.cursor()
    dbres = ""
    try:
        csr.execute(f"SELECT stocksOwned from users WHERE users.firstName = '{userFN}' and users.lastName = '{userLN}'")
        dbres = csr.fetchall()
        print(dbres)
        return dbres
    except Exception as e:
        print(e)


# check if user exists in the users table
def check_user_in_db(userFN, userLN):
    csr = dbc.cursor()
    dbres = ""
    try:
        csr.execute(f"SELECT EXISTS(SELECT firstName, lastName from users WHERE users.firstName = '{userFN}' and users.lastName = '{userLN}')")
        dbres = csr.fetchall()
        # print(dbres)
        for item in dbres:
            # print(item)
            print(item[0])
            return item[0]
    except Exception as e:
        print(e)


# parse the user's stocks from the mySQL database
def parse_user_stocks_from_db(userFN, userLN):
    xv = get_user_stocks(userFN, userLN)    # get database json here
    for item in xv:
        print(item)
        for subitem in item:
            print(subitem)
            tjson = json.loads(subitem)
            print(tjson['stocksOwned'])  # now you have an array with just the user's owned stocks
            stocks = tjson['stocksOwned']
            return stocks   # return the parsed stock list


def get_user_stock_json(userFN, userLN):
    xv = get_user_stocks(userFN, userLN)
    for item in xv:
        for subitem in item:
            print(subitem)
            return subitem

# USER STOCK LIST HERE
# TODO: pass values from login page
# TODO: JWT tokens for login
stock_list = parse_user_stocks_from_db("Abdu", "Alawini")



# gets the company name
def get_company_name(tic):
    stc = Stock(tic, token=iextoken, output_format='pandas')
    preparse = stc.get_company()
    # print(preparse)
    jtemp = DataFrame.to_json(preparse)
    second = json.loads(jtemp)
    companyName = parse_iex_json(second, "companyName")
    return companyName


# gets a list of all stocks in the stock_list above and builds a dictionary with their
# company name, ticker, and current price. called in views.py
# to display current information.
def build_stock_dict():
    final_stock_dict = []
    st = stock_list
    for i in range (0, len(stock_list)):
        dict_item = {
            'stockName': f'{get_company_name(stock_list[i])}',
            'stockTicker': f'{stock_list[i]}',
            'stockPrice': f'{get_stock_price(stock_list[i])}'
        }
        # print(dict_item)
        final_stock_dict.append(dict_item)
    print(final_stock_dict)
    return final_stock_dict


# fetch all from a table. change the execute line to any sql you want
# TODO: make actual database functions better and cooler
def fetch_all_in_table(tb):
    dbres = []
    csr = dbc.cursor()  #dbc in this case is any mysql database connection
    try:
        csr.execute(f"SELECT * FROM {tb}")
        res = csr.fetchall()
        for item in res:
            dbres.append(item)
    except Exception as e:
        print(e)
    print(dbres)
    return dbres


# insert code into IEX database (works for remote mysql, use insert_local() to insert into local sqlite3 db
def insertIEX(tic, price, cchange, cflow, cor, date1):
    csr = dbc.cursor()  #database connection
    csr.execute(f''' INSERT INTO iexdata(ticker, price, cashChange, cashFlow, costOfRevenue, reportDate) VALUES ('{tic}', '{price}', '{cchange}', '{cflow}', '{cor}', '{date1}')''')
    dbc.commit()


# updates existing information in database for fresh data each time
def updateIEX(tic, price, cchange, cflow, cor, date1):
    csr = dbc.cursor()
    csr.execute(f"""
    UPDATE iexdata SET
    price = '{price}',
    cashChange = '{cchange}',
    cashFlow = '{cflow}',
    costOFRevenue = '{cor}',
    reportDate = '{date1}'
    WHERE ticker = '{tic}'
    """)
    dbc.commit()


# gets all users (by firstname and lastname) who own a stock ticker
# advanced SQL functionality
def get_all_users_who_own_stock(tic):
    csr = dbc.cursor()
    try:
        csr.execute(f"""
            SELECT firstName, lastName, stocksOwned
            FROM users WHERE
            stocksOwned like '%{tic}%'
            GROUP BY firstName, lastName, stocksOwned
        """)
        res = csr.fetchall()
        for item in res:
            print(item)
    except Exception as e:
        print(e)


# insert user into a database: works as of 6-22-2020
# IMPORTANT: make sure auto-increment is on in your SQL db for the UserID
def insert_user(userFN, userLN):
    if check_user_in_db(userFN, userLN) == 1:
        return
    else:
        try:
            fresh_stock_json = "{\"stocksOwned\": []}"
            csr = dbc.cursor()
            csr.execute(f"""
                INSERT INTO users(firstName, lastName, stocksOwned)
                VALUES ('{userFN}', '{userLN}', '{fresh_stock_json}')
                """)
            dbc.commit()
        except Exception as e:
            print(e)


# note: need to add user id
def update_user(userFN, userLN, ust):
    csr = dbc.cursor()   # sql connection
    # print(f" ust is {ust}")
    csr.execute(f"""
                UPDATE users SET stocksOwned = '{ust}'
                WHERE firstName = '{userFN}' AND lastName = '{userLN}'
                """)
    dbc.commit()
    print(f"updated {userFN}  {userLN}")


# find the number associated with a tag (i.e cashChange, costOfRevenue)
# from an IEXfinance blob.
def parse_iex_json(jsobj, findKey):
    for item in jsobj.keys():
        # print(f"primary key is {item}")
        for key, val in jsobj[item].items():
            if key == findKey:
                return val


# this func will get stock data from iexfinance and will
# insert it into the database (mysql.)
def get_stock_and_insert_financials(stock_name, udt):
    try:
        tck1 = Stock(stock_name, token=iextoken, output_format='pandas')
        temp_frame = tck1.get_financials()
        temp_json = DataFrame.to_json(temp_frame)
        ugly_json = json.loads(temp_json)   # use this for finding values
        pretty_json = json.dumps(ugly_json, indent=4)
        print(pretty_json)
        tprice = get_stock_price(stock_name)
        tcashflow = parse_iex_json(ugly_json, "cashFlow")
        tcashChange = parse_iex_json(ugly_json, "cashChange")
        tcor = parse_iex_json(ugly_json, "costOfRevenue")
        tdate = parse_iex_json(ugly_json, "reportDate")
        print(tprice)
        print(tcashflow)
        print(tcashChange)
        print(tcor)
        print(tdate)
        if udt == 'i':
            print("inserting into iexdb")
            insertIEX(stock_name, tprice, tcashChange, tcashflow, tcor, tdate) # magic happens here
        if udt == 'u':
            print("updating iexdb")
            updateIEX(stock_name, tprice, tcashChange, tcashflow, tcor, tdate)
        print("check db now")
    except Exception as e:
        print(e)
        print("error inserting into DB")


# ===== non-db functions here =====
"""
    gets the current stock price from a json- just the price.
    
    takes the ticker (all caps) and 
"""
def get_stock_price(ticker):
    final_dict = {}
    stc = Stock(ticker, token=iextoken, output_format='pandas')
    parse = DataFrame.to_json(stc.get_price())
    pretty = json.loads(parse)
    final_dict.update(pretty[ticker])
    print(final_dict[ticker])  # should contain stock price
    return final_dict[ticker]


# add a stock to the user's json. checks if stock ticker is valid
# and also checks if that ticker is in the user's stock list already.
def add_user_stock(userFN, userLN, tic):
    if check_user_in_db(userFN, userLN) == 0:
        print("error: no such user in database")
    else:
        jsc = get_user_stock_json(userFN, userLN)
        good_json = json.loads(jsc)
        print(json.dumps(good_json, indent=4))
        try:
            checkstc = Stock(tic, token=iextoken, output_format='pandas')
            # print(checkstc.get_financials())
        except Exception as e:
            print(e)
            return
        print(good_json['stocksOwned'])
        if tic in good_json['stocksOwned']:
            print("ticker already exists in user json, returning")
            return
        good_json["stocksOwned"].append(tic)
        print(good_json)
        x =  str(good_json).replace("'", "\"")
        print(f" x = \"{x}\"")
        update_user(userFN, userLN, x)


#gets n (default 10) top performing stocks by security score
#returns list of n top performing stocks in desc order.
def get_top_performing_stocks(n=10):
    try:
        csr = dbc.cursor()
        csr.execute(f"""
                SELECT Ticker, SecurityScore
                FROM stocks
                ORDER BY SecurityScore DESC
                LIMIT {n} 
                """)
        res = csr.fetchall()
        return res
    except Exception as e:
        print(e)








#======================= EDIT LOG =================
# [6-14-2020 -- SAYAN] Added functions to take stocks from remote user table
# [6-15-2020 -- SAYAN] Added config yaml file to store SQL user info
# [6-15-2020 -- SAYAN] Added function to update existing database info, works without hassle
# [6-19-2020 -- SAYAN] Made it possible to supply a list of user stocks to the build_stock_dict
# function and generate a view on the webpage from that list.
# [6-19-2020 -- SAYAN] made function parse_user_stocks_from_db to get the list of user stocks from the database
# [6-22-2020 -- SAYAN] added a function to put users into the users DB
# [6-22-2020 -- SAYAN] added a function to check which users own a specific stock
# [6-22-2020 -- KYLE] added a function to get top performing stocks by security score
