"""stonkwebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.urls import include


urlpatterns = [
    path('admin/', admin.site.urls),
    #path('login/', auth_views.LoginView.as_view(template_name=), name='login'),
    #path('logout/', auth_views.LoginView.as_view(), name='logout'),
    path('', include('stockmanager.urls')),
]

# [6/6/2020 --  SAYAN] added the path to thhe URLpatterns to use the stockmanager app that has been created.
# [6-14-2020 -- SAYAN] adding logins to URL patterns, does not currently have a template



